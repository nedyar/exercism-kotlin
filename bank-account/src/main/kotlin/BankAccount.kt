import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger

class BankAccount() {

    private val atomicBalance = AtomicInteger(0)
    val balance: Int
        get() {
           if (!closed.get()) return atomicBalance.toInt()
           else throw IllegalStateException("Closed account")
        }

    private val closed = AtomicBoolean(false)

    fun adjustBalance(delta: Int): Unit {
        if (!closed.get()) {
            this.atomicBalance.getAndAdd(delta)
        }
        else {
            throw IllegalStateException("Closed account")
        }
    }

    fun close(): Unit {
        if (closed.get()) throw IllegalStateException("Already closed")
        else closed.set(true)
    }
}